package samParser;

use strict;
use 5.010;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(pos_analisis, readReferenceGenome, getChromosome, createChromosomesLengths, calcular_tamano, diferencia, deletionSubstring, ancla_derecha, ancla_izq, cigarSplitread, cigarIndel, cigarDeletion, cigarInsertion, cigarMatchMismatch, isIndel);

##Funcion que analiza si la sequencia se sale del 16300
sub pos_analisis
{
	if($_[0]+$_[1] > 16300)
	{
		return 1;	
	}
	else
	{
		return 0;
	}
}


#Funcion que devuelve un string que sera el genoma de referencia
sub readReferenceGenome
{
	##Abrimos el Genoma de regerencia
	open my $F_Genome, '<', $_[0];
	my %refGen;
	my $key;
	while(<$F_Genome>)
	{
		chomp;
		
		if($_ =~ /^\s*>/)
		{
			$key=substr($_, 1, length($_));
			$refGen{$key}=<$F_Genome>;
		}
	}
	close $F_Genome;
	return \%refGen;
}


##Will return the sequence in selected chromosome
sub getChromosome
{	
	##Chromosoma que buscaremos
	my $chromosome=$_[0];
	
	##Referencia a la tabla hash del genoma de referencia
	my $refGenome=$_[1];
	
	##Direccion en memoria de la secuencia para evitar copiado
	my $ptr_value;	
	
	#while ( my ($key) = each($refGenome) )
	foreach my $key (keys %$refGenome)
	{
		$ptr_value = \$refGenome->{$key};

		if( $key eq $chromosome )
		{
			return $ptr_value;
		}
	}

}


##Will return the sequence in selected chromosome
sub getChromosomeLength
{	
	##Chromosoma que buscaremos
	my $chromosome=$_[0];
	
	##Referencia a la tabla hash del genoma de referencia
	my $refLengths=$_[1];
	#while ( my ($key) = each($refLengths) )
	foreach my $key (keys %$refLengths)
	{
		#say "GET $key -> $refLengths->{$key}";
		if( $key eq $chromosome )
		{
			#say "FOUND IT, RETURNING: $refLengths->{$key}";
			return $refLengths->{$key};
		}
	}
	
}

##Get the chromosomes lengths
sub createChromosomesLengths
{
	##Referencia a la tabla hash del genoma de referencia
	my $refGenome=$_[0];
	my %hash;
	
	##Get all the chromosomes lengths
	while ( my ($key) = each($refGenome) )
	{
		$hash{$key}=length($refGenome->{$key});
	}
	
	##Return pointer to hash
	return \%hash;
}


##Funcion que calculara cuantos nucleotidos se extraeran
sub calcular_tamano
{
	my $contador=0;
	my $i=$_[0];
	my $ver=0;
	my $GENOME_LENGTH=$_[2];
	while($ver==0)
	{
		if($i==$GENOME_LENGTH)
		{
			$i=0;
		}
		$i++;
		$contador++;
		if($i==$_[1])
		{
			$ver=1;
		}
	}
	return $contador;
}

##Da los rangos nuevos en caso de que se salga de la posicion $GENOME_LENGTH
sub diferencia
{
	##ARG0: Ultima posicion en el genoma
	##ARG1: Cuanto avanzara
	##ARG2: GENOME length
	my $GENOME_LENGTH=$_[2];
	my $pos=$_[0];   ##16296
	my $avanzar=$_[1];  ##26
	my $suma=$pos+$avanzar;
	my $dif_total=$suma-$GENOME_LENGTH;
	my $dif_hasta_total=$avanzar-$dif_total;
	my @return=($dif_hasta_total,$dif_total);
	return @return;
}                        



##Funcion que del genoma de referencia corta el pedazo deseado
sub deletionSubstring
{
	my $referenceGenome=$_[0];
	my $startPos=$_[1];
	my $forward=$_[2];
	my $GENOME_LENGTH=$_[3];
	my $activator=$_[4];
	my $endPos;
	##Se debe contar las deleciones de que posicion iniciarion y que posicion termino + seq
	if($activator == 1)
	{
		my @dif=diferencia($startPos,$forward, $GENOME_LENGTH);
		$endPos=$dif[1];
	}
	else
	{
		$endPos=$startPos+$forward-1;
	}
	
	##La variable seq (Sequence) se limpia
	my $seq="";
	my $tamano=0;

	##Si llega a pasar la posicion 16300, usaremos la nueva posicion final
	if($startPos>$endPos)
	{
		##se llaman las funciones que calcularan la diferencia y rango
		#my @aux=diferencia($startPos[0],$endPos[1]);	
		$tamano=calcular_tamano($startPos, $endPos, $GENOME_LENGTH);
	}
	else
	{
		$tamano=$endPos-$startPos+1;
	}

	
	##Si esta dentro del rango
	if($startPos+$tamano>$GENOME_LENGTH)
	{
		my ($dif1,$dif2)=diferencia($startPos,$tamano,$GENOME_LENGTH);
		if($dif1==0)
		{
			$dif1=1;
		}
		if($dif2==0)
		{
			$dif2=1;
		}
		else
		{
			$dif2-=1;
		}
		##Extrae el nucleotido
	
		########Aqui es donde hace el substring para conseguir nucleotidos, si algo no concuerda recomiendo intentar ver aqui
		##Seq1 es la secuencia ANTES de la posicion 16300
		my $seq1 = substr($$referenceGenome, $startPos-1, $dif1+1);
	
		##Seq2 es la secuencia DESPUES $GENOME_LENGTH, osea empieza desde la posicion 1
		my $seq2 = substr($$referenceGenome, 1, $dif2);
		
		##Combinamos las secuencias y las hacemos una
		$seq="$seq1$seq2";
	}
	else
	{
		##Extrae el nucleotido
		$seq = substr($$referenceGenome, $startPos-1, $forward);
	}

	return $seq;
}


#########
##Anclas#
#########

sub ancla_izq
{
	my($i_in,$ipos)= @_;
	if($i_in >= $ipos)
	{
		return 1;
	}
	else
	{
		return 0
	}
}

##Verificador de que se alla aprobado los requerimentos del ancla
sub ancla_derecha
{
	##$for_i is the position of the current position in cigar being analyzed
	my ($for_i,$sum,@cigar_ref)=@_;
	my @tmp_ancla=@cigar_ref;
	
	##Caso base, que supere el ancla
	if($sum > 9)
	{
		return 1;
	}
	##De lo contrario va a recorrer recursivamente el cigar en busca de la ancla final
	else
	{
		if($tmp_ancla[$for_i+2] && ($tmp_ancla[$for_i+3] eq "M" || $tmp_ancla[$for_i+3] eq "X" || $tmp_ancla[$for_i+3] eq "=" || $tmp_ancla[$for_i+3] eq "D"))
		{
			return ancla_derecha($for_i+2, $sum+$tmp_ancla[$for_i+2],@tmp_ancla)
		}
		##Si todavia hay mas cigar, entonces seguir explorando
		elsif($tmp_ancla[$for_i+2])
		{
			return ancla_derecha($for_i+2, $sum,@tmp_ancla)
		}
		##Si ya no hay mas cigar y no se cumplio la condicion del ancla, entonces regresa 0
		else
		{
			return 0;
		}		
	}
}


##Verifica si es candidato para INDEL
sub isIndel
{
	##Verificar si es un INDEL

	my ($cigarSize, $i, @cigar)=@_;
	
	##Verifica que haya mas datos en el cigar
	if( $cigarSize > $i + 2)
	{
		##Verify if there is a SplitRead 'N'
		##after the insertion
		if($cigar[$i+3] eq "N")
		{
			##Its an INDEL
			return 1;
		}
	}
	##It is not an INDEL
	return 0;
}



##Devuelve con formato de llave hash, los SplitReads encontrados que pasaron el filtro de las anclas
sub cigarSplitread
{
	my ($referenceGenome, $currentPos, $initialPos, $mutationLen, $i, $GENOME_LENGTH, $activator,@cigar)=@_;
	my $mutationType='SR';
	##Ancla izquierda
	my $init = $currentPos-10;
			
	##Ancla derecha
	my $end = $currentPos+15;

	my $anc_izq=ancla_izq($init,$initialPos);
	my $anc_der=ancla_derecha($i+2, 0, @cigar);
	
	##Si cumple con la regla de las anclas
	if( $anc_izq+$anc_der==2)
	{
		if($cigar[$i] < ($GENOME_LENGTH / 2))
		{
			#Se retorna llave hash de deleciones pero se marca como un SplitRead
			my $splitReadSubstr=deletionSubstring($referenceGenome, $currentPos, $mutationLen, $GENOME_LENGTH, $activator);
			return "$currentPos$mutationType|$mutationLen|$splitReadSubstr";
		}
				
	}


}




##Esta funcion va a conseguir la secuencia del read
##dependiendo lo que indique el CIGAR y de que tamaño
#######################################################
sub cigarIndel
{
	my ($referenceGenome, $insertionSequence, $currentPos, $initialPos, $mutationLen, $GENOME_LENGTH, $i, $activator, @cigar)=@_;
	
	##Ancla izquierda
	my $init = $currentPos-10;
			
	##Ancla derecha
	my $end = $currentPos+10;
	
	my $mutationType="IND";
	my $anc_izq=ancla_izq($init,$initialPos);
	my $anc_der=ancla_derecha($i+2, 0,@cigar);
	##Si cumple con la regla de las anclas
	if( $anc_izq+$anc_der==2)
	{
		my $splitReadSubstr=deletionSubstring($referenceGenome, $currentPos, $mutationLen, $GENOME_LENGTH, $activator);
		my $size=$mutationLen+length($insertionSequence);
		return "$currentPos$mutationType|$size|$insertionSequence+$splitReadSubstr";

	}
	else
	{
		return "";
	}
#			if($activator==1)
#			{
#				$i_last_POS=1;	
#			}
#			if($indel==0)
#			{
				##Se aumenta el contador de inserciones
				#say "Its I and not INDEL and will be mapped now $time";
				##Se manda a guardar
#				mapping_Insertions($s_Insertion_key, $s_Insertion_value);
			
#			}

}


##Recupera del genoma de referencia la secuencia eliminada y da formato a la salida
sub cigarDeletion
{
	my ($referenceGenome, $currentPos, $mutationLen, $GENOME_LENGTH, $activator)=@_;
	my $mutationType='D';
	my $deletionSubstr=deletionSubstring($referenceGenome, $currentPos, $mutationLen, $GENOME_LENGTH, $activator);
	return "$currentPos$mutationType|$mutationLen|$deletionSubstr";
}


##Recupera cadena insertada
sub cigarInsertion
{
	my $currentPos=$_[0];
	my $sequencePosition=$_[1];
	my $mutation=$_[2];
	my $sequence=$_[3];	
	my $length=$_[4];
	my $substring=substr $sequence, $sequencePosition, $length;	
	my $key="$currentPos$mutation|$length";
	my $value=$substring;
	
	##Se consigue la pasicion donde empieza la insercion
	my $i_Insertion_pos=$currentPos;
	my $i_sequence_position+=$length;
			
	##Verifica que no haya ocurrido un INDEL
	my $indel=0;
			
	
	if ($indel==1)
	{
		return "Indel";
	}
	
	return "$key|$value";		
	
}






##Esta funcion va a conseguir la secuencia del read
##dependiendo lo que indique el CIGAR y de que tamaño
##Parametros:
#	0:Posicion actual en el genoma
#	1:Posicion actual en la secuencia del read
#	2:Tipo Mutacion
#	3:Secuencia del read
#	4:Tamano de la mutacion indicado por el cigar
#######################################################
sub cigarMatchMismatch
{
	my $currentPos=$_[0];
	my $sequencePosition=$_[1];
	my $mutation=$_[2];
	my $sequence=$_[3];	
	my $length=$_[4];
	my $substring=substr $sequence, $sequencePosition, $length;	
	my $key="$currentPos$mutation|$length";
	my $value=$substring;
	return "$key|$value";		


}

